# Сценарии для Train Simulator 2021 x64

##### Как установить

  - Нажать кнопку "Download" сверху слева возле голубой кнопки "Clone", и выбрать желаемый формат архива.
  - Распковать архив, и с помощью `Utilities.exe`, который поставляется с игрой, установить `.rwp`-пакет.


##### Зависимости

###### Shevchenko-Tsvetkovo-v3.1

  - [Маршрут](http://www.railunion.net/forum53/topic13743.html#p227441)
  - [5_RU_TSP_BoxCar_1](https://yadi.sk/d/Brs6gXbIqpuyYQ)
  - [Пак вагонов-думпкаров, версия 1.1f](http://www.railunion.net/forum149/topic12240.html#p214200)
  - [3ТЭ10М (DSB_3TE10_v.1.0_120820)](http://www.railunion.net/forum52/topic15879-195.html#p243873), [патч для системы охлаждения тепловоза](http://www.railunion.net/forum52/topic15879-150.html#p248053)
  - [[1.2] Пак химических цистерн СССР (DSB_Chemical_Tankers_USSR_v.1.2_210620)](http://www.railunion.net/forum149/topic15912.html)
  - [ЭР1-159 (ER1-159)](http://www.railunion.net/forum52/topic13511-90.html#p225470)
  - [Вагон Хоппер модель 19-739 (Hopper_19-739(2 шт))](http://www.railunion.net/forum149/topic12240-75.html#p214200)
  - [Вагон Хоппер модель 19-752 (Hopper_19-752(2 шт))](http://www.railunion.net/forum149/topic12240-75.html#p214200)
  - [Платформа 13-401: пакет №1 (RRS_Flatcar_13-401_Pack1)](http://railroadsim.net/ru/downloads/rw/cars/flatcars)
  - [Пак крытых вагонов, версия 1.3 (RRS_RU_Boxcars Pack v1.3)](http://www.railunion.net/forum149/topic12240-75.html)
  - [Полувагоны, версия 1.1 Модель 12-753 (RRS_RU_Gondola_12-753 v.1.0)](http://www.railunion.net/forum149/topic13094-15.html#p222537)
  - [Тепловоз ТЭМ2: пакет №1 (RRS_TEM2_Pack1)](http://railroadsim.net/ru/downloads/rw/packs)
  - [Вагон-термос: пакет №1 (RRS_Thermos_Pack1)](http://railroadsim.net/ru/downloads/rw/cars/thermos)
  - [Пассажирские вагоны плацкартные (Плацкартные Вагоны Старые (15 шт))](http://www.railunion.net/forum149/topic12240-90.html#p214200)
  - [Цистерна 15-150 #50260769 (Цистерна 50260769)](http://www.railunion.net/forum149/topic12240-90.html#p214200)
  - [Цистерны 15-150 (Цистерны_15-150(5 шт))](http://www.railunion.net/forum149/topic12240-90.html#p214200)
